<?php

namespace App\Services\Email;

use Mail;
use App\Models\Access\User\User;
use App\Models\Access\Role\Role;
use App\Models\Eventos\Eventos;
use App\Models\Eventos\EventosNotificaciones;
use Carbon\Carbon;

class Correo
{
    public static function enviar($destinatarios, $asunto, $mensaje)
    {
        foreach ($destinatarios as $destinatario) {
            Mail::send('emails.correo', ['destinatario' => $destinatario, 'asunto' => $asunto, 'mensaje' => $mensaje], function ($msj) use ($destinatario, $asunto) {
                $msj->subject($asunto);

                $msj->to($destinatario['email'], $destinatario['name']);
            });
        }
    }

    public static function registrar($id_evento)
    {
        $notificacion = new EventosNotificaciones();

        $notificacion->id_evento = $id_evento;
        $notificacion->fecha = Carbon::now();
        $notificacion->envio_email = 1;

        $notificacion->save();
    }

    public static function notificacion($codigo, $argumentos = [])
    {
    	$evento = Eventos::select('id', 'asunto', 'mensaje', 'destinatarios')
    	->where('cod_evento', $codigo)
    	->where('estatus', 1)
    	->where('correo', 1)
    	->first();

    	if (!is_null($evento)) {
    		$destinatarios = self::destinararios($evento);

	    	self::enviar($destinatarios, $evento->asunto, $evento->mensaje);
	    	self::registrar($evento->id);

	    	return true;
    	}

    	return false;
    }

    public static function destinararios($evento)
    {
    	$destinatarios = $evento->destinatarios;

    	$roles = self::buscarPorRoles($destinatarios->roles);
    	$usuarios = self::buscarPorUsuarios($destinatarios->usuarios);
    	$correos = self::buscarPorCorreos($destinatarios->correos);

    	return array_merge($roles, $usuarios, $correos);
    }

    public static function buscarPorRoles($roles)
    {
    	if (is_null($roles)) {
    		return [];
    	}

    	return User::where('status', 1)
        ->whereHas('roles', function ($query) use ($roles) {
            $query->whereIn('id', $roles);
        })
        ->get(['users.email', 'users.name'])
        ->toArray();
    }

    public static function buscarPorUsuarios($usuarios)
    {
    	if (is_null($usuarios)) {
    		return [];
    	}

    	return User::where('status', 1)
    	->whereIn('id', $usuarios)
        ->get(['email', 'name'])
        ->toArray();
    }

    public static function buscarPorCorreos($correos)
    {
    	if (is_null($correos)) {
    		return [];
    	}

    	$usuarios = [];

    	foreach ($correos as $correo) {
    		$usuarios[] = [
    			'email' => $correo,
    			'name' => null,
    		];
    	}

    	return $usuarios;
    }
}