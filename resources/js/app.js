/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 /** ----------------------- FILTROS -------------------------- */

   Vue.filter('number', function (amount, decimales = 0) { //transforma un numero a formato numerico
    if (amount == null) { amount = 0; }
    const amt = Number(amount);
    return amt.toLocaleString(undefined, {minimumFractionDigits: decimales, maximumFractionDigits: decimales});
  });
  
  Vue.filter('date', function (str, format) { //formatea la fecha al formato
    if (str == null) { return '(n/a)'; }

    switch (format) {
      case 'YYYY-MM-DD HH:MM:SS': //recibe formato YYYYMMDDHHMISS, imprime YYYY-MM-DD HH:MM:SS
        return str.substr(0,4)+'-'+str.substr(4,2)+'-'+str.substr(6,2)+' '+str.substr(8,2)+':'+str.substr(10,2)+':'+str.substr(12,2);
        break;
      case 'HH:MM:SS': //recibe formato HHMISS, imprime HH:MM:SS
        return str.substr(0,2)+':'+str.substr(2,2)+':'+str.substr(4,2);
        break;
      case 'HH:MM:SS2': //recibe formato YYYYMMDDHHMISS, imprime HH:MM:SS
        return str.substr(8,2)+':'+str.substr(10,2)+':'+str.substr(12,2);
        break;
      case 'YYYY-MM-DD': //recibe formato YYYYMMDDHHMISS o YYYYMMDD, imprime YYYY-MM-DD
        return str.substr(0,4)+'-'+str.substr(4,2)+'-'+str.substr(6,2);
        break;
      case 'YYYY-MM-DD 00:00:00': //recibe formato YYYY-MM-DD 00:00:00, imprime YYYY-MM-DD
        return str.substr(0,4)+'-'+str.substr(5,2)+'-'+str.substr(8,2);
        break;
      default: //recibe formato YYYYMMDDHHMISS o YYYYMMDD, imprime YYYY-MM-DD
        return str.substr(0,4)+'-'+str.substr(4,2)+'-'+str.substr(6,2);
        break;
    }

  });

/** ----------------------- FINAL DE FILTROS -------------------------- */


const app = new Vue({
  el: '#app'
});
