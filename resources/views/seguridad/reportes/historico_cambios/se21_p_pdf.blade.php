{{-- @Nombre del programa: Vista de Excel  Reporte de Usuarios --}}
{{-- @Funcion: Descargar el reporte de usuarios registrados en excel --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 30/05/2018 --}}
{{-- @Requerimiento:  --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}
<!DOCTYPE>
<html>
<head>
  <title>REPORTE USUARIOS</title>
  {!! Html::style('css/AdminLTE.css') !!}
  {!! Html::style('css/pdf.css') !!}
<body>
<p>Banco Central de Venezuela</p>
<p>Gerencia de Tesorería</p>
<p>Departamento Cámara de Compensación Electrónica</p>
<h4 align=center>CÁMARA DE COMPENSACIÓN - REPORTE DE HISTORICO DE CAMBIOS</h4>
<br>
<div class="row">
<table border="0" width="100%" cellspacing="0" cellpadding="5">
  <thead style="background-color: #C2E7FC;">
  <tr>
    <th>Usuario</th>
    <th>Fecha</th>
    <th>IP</th>
    <th>Acción</th>
    <th>Ruta</th>   
  </tr>
  </thead>  
  <tbody>
    @foreach ($logs as $log)
    <tr>
      <td>{{ $log->username }}</td>
      <td>{{ $log->created_at }}</td>
      <td>{{ $log->ip_address }}</td>
      <td>{{ $log->event }}</td>
      <td>{{ $log->url }}<td/>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
</body>
</html>



    
  