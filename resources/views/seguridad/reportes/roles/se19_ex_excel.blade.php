{{-- @Nombre del programa: Vista de Excel  Reporte de Usuarios --}}
{{-- @Funcion: Descargar el reporte de usuarios registrados en excel --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 30/05/2018 --}}
{{-- @Requerimiento:  --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}


<table>
  <tr>
   <td>Banco Central de Venezuela</td>
  </tr>
  <tr>
   <td>Gerencia de Tesorería</td>
  </tr>
  <tr>
   <td>Departamento Cámara de Compensación Electrónica</td>
  </tr>
 <tr>
    <th colspan="6" style="text-align: center;">CÁMARA DE COMPENSACIÓN - REPORTE DE ROLES</th>
</tr>
 <tr>
    <th colspan="6" style="text-align: center;">ROLES</th>
</tr>
<tr>
<th colspan="1" style="text-align: center;">FECHA  {{Date::now()->format('d-m-Y')  }} </th>
  </tr>
 
</table >
<table >
  <tr style="background-color: #C2E7FC">
    <th>Nombre</th>
    <th>Usuarios </th>
    <th>Permisos </th>
   
  </tr>
  <tbody>
    @foreach ($roles as $role)
    <tr>
      <td>{{ $role->name }}</td>
      <td>{{$role->users->pluck('name')->implode(', ') }}</td>
      <td>{{ $role->todos != 0 ? 'Todos' : $role->permissions->pluck('display_name')->implode(', ') }}</td>
     
    </tr>
    @endforeach
  </tbody>
</table>